package maja.service.impl;

import maja.dao.BookDAO;
import maja.exeption.BookExeption;
import maja.model.BookModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.awt.print.Book;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BookServiceImplTest {

    @InjectMocks
    private BookServiceImpl bookService;

    @Mock
    private BookDAO bookDAO;

    @Test
    public void shouldReturnBookById() {
        BookModel bookModel = new BookModel();
        when(bookDAO.findById(1)).thenReturn(Optional.of(bookModel));

        assertEquals(bookModel, bookService.getBookById(1));
    }

    @Test(expected = BookExeption.class)
    public void shouldThrowBookExceptionWhenBookNotFoundById() {
        doThrow(new BookExeption("")).when(bookDAO).findById(1);

        bookService.getBookById(1);
        fail("Should throw book exception because book by id is not found");
    }

}
