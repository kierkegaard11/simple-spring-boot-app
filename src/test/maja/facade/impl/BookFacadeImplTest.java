package maja.facade.impl;

import maja.converter.Converter;
import maja.dto.BookDTO;
import maja.model.BookModel;
import maja.populator.Populator;
import maja.reverseConverter.ReverseConverter;
import maja.reversePopulator.ReversePopulator;
import maja.service.BookService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class BookFacadeImplTest {

    @InjectMocks
    private BookFacadeImpl bookFacade;

    @Mock
    private BookService bookService;

    @Mock
    private Converter bookConverter;

    @Mock
    private Populator defaultBookPopulator;

    @Mock
    private Populator simpleBookPopulator;

    @Mock
    private ReversePopulator defaultBookReversePopulator;

    @Mock
    private ReverseConverter bookReverseConverter;

    @Test
    public void shouldReturnAllBooks() {
        BookModel bookModel = new BookModel();
        BookDTO bookDTO = new BookDTO();
        when(bookService.getAllBooks()).thenReturn(Collections.singletonList(bookModel));
        when(bookConverter.convert(bookModel)).thenReturn(bookDTO);

        assertEquals(Collections.singletonList(bookDTO), bookFacade.getAllBooks());
        verify(bookConverter, times(1)).setPopulator(simpleBookPopulator);
    }

    @Test
    public void shouldReturnEmptyBookList() {
        when(bookService.getAllBooks()).thenReturn(Collections.emptyList());

        assertEquals(Collections.EMPTY_LIST, bookFacade.getAllBooks());
        verify(bookConverter, times(1)).setPopulator(simpleBookPopulator);
        verify(bookConverter, times(0)).convert(any(BookModel.class));
    }

    @Test
    public void shouldSaveBook() {
        BookDTO bookDTO = new BookDTO();
        BookModel bookModel = new BookModel();
        when(bookReverseConverter.convert(bookDTO)).thenReturn(bookModel);

        bookFacade.saveBook(bookDTO);
        verify(bookReverseConverter, times(1)).setReversePopulator(defaultBookReversePopulator);
        verify(bookService, times(1)).saveBook(bookModel);
    }

    @Test
    public void shouldUpdateBook(){
        BookDTO bookDTO = new BookDTO();
        BookModel bookModel = new BookModel();
        when(bookReverseConverter.convert(bookDTO)).thenReturn(bookModel);

        bookFacade.updateBook(bookDTO);
        verify(bookReverseConverter, times(1)).setReversePopulator(defaultBookReversePopulator);
        verify(bookService, times(1)).updateBook(bookModel);
    }

    @Test
    public void shouldReturnBookById(){
        BookModel bookModel = new BookModel();
        BookDTO bookDTO = new BookDTO();
        when(bookService.getBookById(1)).thenReturn(bookModel);
        when(bookConverter.convert(bookModel)).thenReturn(bookDTO);

        assertEquals(bookFacade.getBookById(1), bookDTO);//treba obrnut redosled parametara
        verify(bookConverter,times(1)).setPopulator(defaultBookPopulator);
    }
}
