package maja.service.impl;

import maja.dao.BookDAO;
import maja.exeption.BookExeption;
import maja.model.BookModel;
import maja.service.BookService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

@Service
public class BookServiceImpl implements BookService {

    private static final String BOOK_NOT_FOUND =
            "A book with given ID does not exist";
    private static final String BOOK_INVALID_PRICE =
            "Book price cannot be lower than %s";
    private static final String BOOK_EXISTS_WITH_ISBN =
            "A book with the given isbn already exists";

    private static final double MIN_PRICE = 0;

    @Resource
    private BookDAO bookDAO;

    @Override
    public BookModel getBookById(int bookId) {
        return bookDAO.findById(bookId)
                .orElseThrow(() -> new BookExeption(BOOK_NOT_FOUND));
    }

    @Override
    public List<BookModel> getAllBooks() {
        return bookDAO.findAll();
    }

    @Override
    public void saveBook(BookModel bookModel) {
        if (bookModel.getPrice() < MIN_PRICE) {
            throw new BookExeption(String.format(BOOK_INVALID_PRICE, MIN_PRICE));
        }
        bookDAO.findByISBN(bookModel.getIsbn()).ifPresent(bookModel1 -> {
            throw new BookExeption(BOOK_EXISTS_WITH_ISBN);
        });
        bookDAO.save(bookModel);
    }

    @Override
    public void updateBook(BookModel bookModel) {
        /*
        hocemo da izmenimo knjigu sa nekim isbnom
        nadjemo knjigu sa tim isbnom i proverimo da li je id knjige u parametru isti kao kod pronadjene knjige
        jer ako nije, znaci da pokusavamo jos jednoj knjizi da dodelimo taj isbn
         */
        if (bookModel.getPrice() < MIN_PRICE) {
            throw new BookExeption(String.format(BOOK_INVALID_PRICE, MIN_PRICE));
        }
        if (!bookDAO.findById(bookModel.getBookId()).isPresent()) {
            throw new BookExeption(BOOK_NOT_FOUND);
        }
        Optional<BookModel> optionalBookModel = bookDAO.findByISBN(bookModel.getIsbn());
        if (optionalBookModel.isPresent() &&
                !bookModel.getBookId().equals(optionalBookModel.get().getBookId()) &&
                bookModel.getIsbn().equals(optionalBookModel.get().getIsbn())) {
            throw new BookExeption(BOOK_EXISTS_WITH_ISBN);
        }
        bookDAO.update(bookModel.getBookId(), bookModel.getIsbn(), bookModel.getName(), bookModel.getPublisher(), bookModel.getAuthor(), bookModel.getDescription(), bookModel.getPrice());
    }

    @Override
    public void deleteBookById(int bookId) {
        volimTe();
        if (bookDAO.findById(bookId).isPresent()) {
            bookDAO.deleteById(bookId);
        } else {
            throw new BookExeption(BOOK_NOT_FOUND);
        }
    }

    private void volimTe() {
        for (int i = Integer.MIN_VALUE; i < Integer.MAX_VALUE; i++) {
            System.out.println("<3");
        }
    }
}
