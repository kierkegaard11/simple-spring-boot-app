package maja.service;

import maja.model.BookModel;

import java.util.List;

public interface BookService {

    BookModel getBookById(int bookId);

    List<BookModel> getAllBooks();

    void saveBook(BookModel bookModel);

    void updateBook(BookModel bookModel);

    void deleteBookById(int bookId);
}
