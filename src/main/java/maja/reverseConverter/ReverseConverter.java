package maja.reverseConverter;

import maja.dto.GenericDTO;
import maja.model.GenericModel;
import maja.reversePopulator.ReversePopulator;

public abstract class ReverseConverter {

    private ReversePopulator reversePopulator;

    public GenericModel convert(GenericDTO source){
       GenericModel target = createModel();
       reversePopulator.populate(source, target);
       return target;
    }

    public void setReversePopulator(ReversePopulator populator) {
        reversePopulator = populator;
    }

    protected abstract GenericModel createModel();
}
