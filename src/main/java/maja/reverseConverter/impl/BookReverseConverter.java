package maja.reverseConverter.impl;

import maja.model.BookModel;
import maja.model.GenericModel;
import maja.reverseConverter.ReverseConverter;
import org.springframework.stereotype.Service;

@Service
public class BookReverseConverter extends ReverseConverter {

    @Override
    protected GenericModel createModel() {
        return new BookModel();
    }
}
