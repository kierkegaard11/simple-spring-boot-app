package maja.controller;

import maja.dto.BookDTO;
import maja.exeption.BookExeption;
import maja.facade.BookFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/books")
public class BookController {

    private static final Logger LOGGER = LoggerFactory.getLogger(BookController.class);

    private static final String GET_ALL_BOOKS_ERROR =
            "Unexpected error occurred while getting all books";
    private static final String GET_BOOK_BY_ID_ERROR =
            "Unexpected error occurred while getting book by id";
    private static final String SAVE_BOOK_SUCCESS =
            "Book has been successfully saved";
    private static final String SAVE_BOOK_ERROR =
            "Unexpected error occurred while saving the book";
    private static final String UPDATE_BOOK_SUCCESS =
            "Book has been successfully updated";
    private static final String UPDATE_BOOK_ERROR =
            "Unexpected error occurred while updating the book";
    private static final String DELETE_BOOK_SUCCESS =
            "Book has been successfully deleted";
    private static final String DELETE_BOOK_ERROR =
            "Unexpected error occured while deleting the book";

    @Resource
    private BookFacade bookFacade;

    @GetMapping
    public ResponseEntity<?> getAllBooks() {
        try {
            List<BookDTO> books = bookFacade.getAllBooks();
            return ResponseEntity.ok(books);
        } catch (RuntimeException e) {
            LOGGER.warn(GET_ALL_BOOKS_ERROR, e);
            return new ResponseEntity<>(GET_ALL_BOOKS_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/{bookId}")
    public ResponseEntity<?> getBookById(@PathVariable Integer bookId) {
        try {
            BookDTO book = bookFacade.getBookById(bookId);
            return ResponseEntity.ok(book);
        } catch (BookExeption e) {
            LOGGER.warn(e.getMessage(), e);
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (RuntimeException e) {
            LOGGER.warn(GET_BOOK_BY_ID_ERROR, e);
            return new ResponseEntity<>(GET_BOOK_BY_ID_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping
    public ResponseEntity<?> saveBook(@RequestBody BookDTO bookDTO) {
        try {
            bookFacade.saveBook(bookDTO);
            return ResponseEntity.ok(SAVE_BOOK_SUCCESS);
        } catch (BookExeption e) {
            LOGGER.warn(e.getMessage(), e);
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (RuntimeException e) {
            LOGGER.warn(SAVE_BOOK_ERROR, e);
            return new ResponseEntity<>(SAVE_BOOK_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping
    public ResponseEntity<?> updateBook(@RequestBody BookDTO bookDTO) {
        try {
            bookFacade.updateBook(bookDTO);
            return ResponseEntity.ok(UPDATE_BOOK_SUCCESS);
        } catch (BookExeption e) {
            LOGGER.warn(e.getMessage(), e);
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (RuntimeException e) {
            LOGGER.warn(UPDATE_BOOK_ERROR, e);
            return new ResponseEntity<>(UPDATE_BOOK_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping(value = "/{bookId}")
    public ResponseEntity<?> deleteBookById(@PathVariable Integer bookId) {
        try {
            bookFacade.deleteBookById(bookId);
            return ResponseEntity.ok(DELETE_BOOK_SUCCESS);
        } catch (BookExeption e) {
            LOGGER.warn(e.getMessage(), e);
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (RuntimeException e) {
            LOGGER.warn(DELETE_BOOK_ERROR, e);
            return new ResponseEntity<>(DELETE_BOOK_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
