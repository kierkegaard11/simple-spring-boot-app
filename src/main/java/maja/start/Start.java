package maja.start;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@EnableWebMvc
@EntityScan(basePackages = "maja.model")
@EnableJpaRepositories(basePackages = "maja.dao")
@SpringBootApplication
@ComponentScan(basePackages = "maja")
public class Start {

    public static void main(String[] args) {
        SpringApplication.run(Start.class, args);
    }
}
