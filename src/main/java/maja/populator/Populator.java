package maja.populator;

import maja.dto.GenericDTO;
import maja.model.GenericModel;

public interface Populator {

    void populate(GenericModel source, GenericDTO target);
}
