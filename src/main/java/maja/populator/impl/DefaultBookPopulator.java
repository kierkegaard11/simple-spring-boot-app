package maja.populator.impl;

import maja.dto.BookDTO;
import maja.dto.GenericDTO;
import maja.model.BookModel;
import maja.model.GenericModel;
import maja.populator.Populator;
import org.springframework.stereotype.Service;

@Service
public class DefaultBookPopulator implements Populator {

    @Override
    public void populate(GenericModel source, GenericDTO target) {
        BookModel bookModel = (BookModel) source;
        BookDTO bookDTO = (BookDTO) target;

        bookDTO.setBookId(bookModel.getBookId());
        bookDTO.setIsbn(bookModel.getIsbn());
        bookDTO.setName(bookModel.getName());
        bookDTO.setAuthor(bookModel.getAuthor());
        bookDTO.setPublisher(bookModel.getPublisher());
        bookDTO.setDescription(bookModel.getDescription());
        bookDTO.setPrice(bookModel.getPrice());
    }
}
