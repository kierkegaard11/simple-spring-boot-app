package maja.populator.impl;

import maja.dto.BookDTO;
import maja.dto.GenericDTO;
import maja.model.BookModel;
import maja.model.GenericModel;
import maja.populator.Populator;
import org.springframework.stereotype.Service;

@Service
public class SimpleBookPopulator implements Populator {

    @Override
    public void populate(GenericModel source, GenericDTO target) {
        BookDTO bookDTO = (BookDTO) target;
        BookModel bookModel = (BookModel) source;

        bookDTO.setBookId(bookModel.getBookId());
        bookDTO.setName(bookModel.getName());
        bookDTO.setAuthor(bookModel.getAuthor());
        bookDTO.setPrice(bookModel.getPrice());
    }
}
