package maja.converter;

import maja.dto.GenericDTO;
import maja.model.GenericModel;
import maja.populator.Populator;

public abstract class Converter {

    private Populator populator;

    public GenericDTO convert(GenericModel source) {
        GenericDTO target = createDTO();
        populator.populate(source, target);
        return target;
    }

    public void setPopulator(Populator populator) {
        this.populator = populator;
    }

    protected abstract GenericDTO createDTO();
}
