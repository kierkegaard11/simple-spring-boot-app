package maja.converter.impl;

import maja.converter.Converter;
import maja.dto.BookDTO;
import maja.dto.GenericDTO;
import org.springframework.stereotype.Service;

@Service
public class BookConverter extends Converter {

    @Override
    protected GenericDTO createDTO() {
        return new BookDTO();
    }
}
