package maja.reversePopulator.impl;

import maja.dto.BookDTO;
import maja.dto.GenericDTO;
import maja.model.BookModel;
import maja.model.GenericModel;
import maja.reversePopulator.ReversePopulator;
import org.springframework.stereotype.Service;

@Service
class DefaultBookReversePopulator implements ReversePopulator {

    @Override
    public void populate(GenericDTO source, GenericModel target) {
        BookDTO bookDTO = (BookDTO) source;
        BookModel bookModel = (BookModel) target;

        bookModel.setBookId(bookDTO.getBookId());
        bookModel.setIsbn(bookDTO.getIsbn());
        bookModel.setName(bookDTO.getName());
        bookModel.setAuthor(bookDTO.getAuthor());
        bookModel.setPublisher(bookDTO.getPublisher());
        bookModel.setDescription(bookDTO.getDescription());
        bookModel.setPrice(bookDTO.getPrice());
    }
}
