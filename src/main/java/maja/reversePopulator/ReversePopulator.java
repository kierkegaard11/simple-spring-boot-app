package maja.reversePopulator;

import maja.dto.GenericDTO;
import maja.model.GenericModel;

public interface ReversePopulator {

    void populate(GenericDTO source, GenericModel target);
}
