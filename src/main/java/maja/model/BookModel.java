package maja.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "book", schema = "books")
public class BookModel implements GenericModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "book_id")
    private Integer bookId;

    @Basic
    @NotNull
    @Column(name = "isbn", nullable=false)
    private String isbn;

    @Basic
    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Basic
    @NotNull
    @Column(name = "author", nullable=false)
    private String author;

    @Basic
    @NotNull
    @Column(name = "publisher", nullable=false)
    private String publisher;

    @Basic
    @NotNull
    @Column(name = "description",nullable = false)
    private String description;

    @Basic
    @NotNull
    @Column(name = "price", nullable = false)
    private Double price;

    public BookModel(){
    }

    public Integer getBookId() {
        return bookId;
    }

    public void setBookId(Integer bookId) {
        this.bookId = bookId;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
