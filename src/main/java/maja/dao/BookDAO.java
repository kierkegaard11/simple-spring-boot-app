package maja.dao;

import maja.model.BookModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Optional;

@Repository
public interface BookDAO extends JpaRepository<BookModel, Integer> {

    @Transactional
    @Modifying
    @Query("UPDATE BookModel b SET b.name = ?3, b.isbn = ?2, b.publisher = ?4, b.author = ?5, b.description = ?6, b.price = ?7 WHERE b.bookId = ?1")
    void update(int bookId, String isbn, String name, String publisher, String author, String description, Double price);

    @Query("SELECT b FROM BookModel b WHERE b.isbn = ?1")
    Optional<BookModel> findByISBN(String isbn);
}
