package maja.facade.impl;

import maja.converter.Converter;
import maja.dto.BookDTO;
import maja.facade.BookFacade;
import maja.model.BookModel;
import maja.populator.Populator;
import maja.reverseConverter.ReverseConverter;
import maja.reversePopulator.ReversePopulator;
import maja.service.BookService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
public class BookFacadeImpl implements BookFacade {

    @Resource
    private BookService bookService;

    @Resource
    private Converter bookConverter;

    @Resource
    private Populator defaultBookPopulator;

    @Resource
    private Populator simpleBookPopulator;

    @Resource
    private ReversePopulator defaultBookReversePopulator;

    @Resource
    private ReverseConverter bookReverseConverter;

    @Override
    public BookDTO getBookById(int bookId) {
        bookConverter.setPopulator(defaultBookPopulator);
        BookModel bookModel = bookService.getBookById(bookId);
        return (BookDTO) bookConverter.convert(bookModel);
    }

    @Override
    public List<BookDTO> getAllBooks() {
        bookConverter.setPopulator(simpleBookPopulator);
        List<BookModel> books = bookService.getAllBooks();
        return books.stream()
                .map(bookModel -> (BookDTO) bookConverter.convert(bookModel))
                .collect(toList());
        /*
       List<BookDTO> booksDTO = new LinkedList<BookDTO>();
        for (BookModel bm:books) {
            BookDTO bookDTO = (BookDTO) bookConverterImpl.convert(bm);
            booksDTO.add(bookDTO);
        }
        return booksDTO;
        */
    }

    @Override
    public void saveBook(BookDTO bookDTO) {
        bookReverseConverter.setReversePopulator(defaultBookReversePopulator);
        BookModel bookModel = (BookModel) bookReverseConverter.convert(bookDTO);
        bookService.saveBook(bookModel);
    }

    @Override
    public void updateBook(BookDTO bookDTO) {
        bookReverseConverter.setReversePopulator(defaultBookReversePopulator);
        BookModel bookModel = (BookModel) bookReverseConverter.convert(bookDTO);
        bookService.updateBook(bookModel);
    }

    @Override
    public void deleteBookById(int bookId) {
        bookService.deleteBookById(bookId);
    }
}
