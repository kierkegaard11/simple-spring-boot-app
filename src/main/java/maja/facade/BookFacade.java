package maja.facade;

import maja.dto.BookDTO;

import java.util.List;

public interface BookFacade {

    BookDTO getBookById(int bookId);

    List<BookDTO> getAllBooks();

    void saveBook(BookDTO bookDTO);

    void updateBook(BookDTO bookDTO);

    void deleteBookById(int bookId);
}
